import 'package:flutter/material.dart';
import 'package:financial_assistant/BudgetingPageView.dart';

class BudgetingPage extends StatefulWidget {
  static String tag = 'budgeting-page';

  @override
  BudgetingPageView createState() => new BudgetingPageView();
}

abstract class BudgetingPageState extends State<BudgetingPage> {}
