import 'package:flutter/material.dart';
import 'package:financial_assistant/RegisterPage.dart';
import 'package:financial_assistant/RegistrationForm.dart';

class RegisterPageView extends RegisterPageState {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Register'),
        centerTitle: true,
        elevation: 0.0,
      ),
      body: const SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 8.0),
          child: RegistrationForm(),
        ),
      ),
    );
  }
}
