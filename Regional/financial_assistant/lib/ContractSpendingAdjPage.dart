import 'package:flutter/material.dart';
import 'package:financial_assistant/ContractSpendingAdjPageView.dart';

class ContractSpendingAdjPage extends StatefulWidget {
  static String tag = 'spending-adj-page';

  @override
  ContractSpendingAdjPageView createState() =>
      new ContractSpendingAdjPageView();
}

abstract class ContractSpendingAdjPageState
    extends State<ContractSpendingAdjPage> {}
