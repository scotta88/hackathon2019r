import 'package:flutter/material.dart';
import 'package:financial_assistant/NavigationDrawerView.dart';

class NavigationDrawer extends StatefulWidget {
  @override
  NavigationDrawerView createState() => new NavigationDrawerView();
}

abstract class NavigationDrawerState extends State<NavigationDrawer> {}
