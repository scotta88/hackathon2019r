import 'package:flutter/material.dart';
import 'DashboardPageView.dart';
import 'package:flutter_circular_chart/flutter_circular_chart.dart';
import 'framework_packages/globals.dart' as globals;

class DashboardPage extends StatefulWidget {
  static String tag = 'dashboard-page';

  @override
  DashboardPageView createState() => new DashboardPageView();
}

abstract class DashboardPageState extends State<DashboardPage> {
  var spending;

  final GlobalKey<AnimatedCircularChartState> chartKey =
      new GlobalKey<AnimatedCircularChartState>();

  List<CircularStackEntry> initialChartData = <CircularStackEntry>[
    new CircularStackEntry(
      <CircularSegmentEntry>[
        new CircularSegmentEntry(
            30.0, Colors.green[200], rankKey: 'Q1'),
        new CircularSegmentEntry(
            190.0, Colors.red[200], rankKey: 'Q2'),
        new CircularSegmentEntry(
            150.0, Colors.blue[200], rankKey: 'Q3'),
        new CircularSegmentEntry(350.0, Colors.yellow[200], rankKey: 'Q4'),
        new CircularSegmentEntry(
            30.0, Colors.deepOrange[200], rankKey: 'Q5'),
        new CircularSegmentEntry(45.0, Colors.brown[200], rankKey: 'Q6'),
        new CircularSegmentEntry(
            15.0, Colors.blueGrey[200], rankKey: 'Q7'),
        new CircularSegmentEntry(
            0.0, Colors.lightBlueAccent[200], rankKey: 'Q8')
      ],
      rankKey: 'Quarterly Profits',
    ),
  ];

  getAprilSpendings(){
    globals.getAllTransactions();
    int shopping = 0;
    int food = 0;
    int house = 0;
    int education = 0;
    int travel = 0;
    int personal = 0;
    int investment = 0;
    for(var transaction in globals.transactionsList){
      var date_array = transaction.date.split("/");
      var dateTime = new DateTime(int.parse(date_array[2]), int.parse(date_array[1]),int.parse(date_array[0]));
      if(dateTime.compareTo(new DateTime(2019, 4, 1)) >= 0 ){
        if(transaction.category == "shopping"){
          shopping += transaction.amount;
        }else if(transaction.category == "food/liquor"){
          food+= transaction.amount;
        }else if(transaction.category == "household"){
          house+= transaction.amount;
        }else if(transaction.category == "education"){
          education+= transaction.amount;
        }else if(transaction.category == "travel"){
          travel+= transaction.amount;
        }else if(transaction.category == "personal_care"){
          personal+= transaction.amount;
        }else if(transaction.category == "investment"){
          investment+= transaction.amount;
        }
      }
    }
    return [shopping, food, house, education, travel, personal, investment];
  }

  getUpcomingPayments(){
    globals.getAllTransactions();
    if(globals.transactionsList != null && globals.transactionsList.length > 0) {
      globals.upcomingElectricityBill =
          globals.transactionsList.where((i) => i.item == 'electricity' &&
              i.month == 3 && i.year == 2019).toList()[0].amount;
      globals.upcomingRentBill =
          globals.transactionsList.where((i) => i.item == 'rent' &&
              i.month == 3 && i.year == 2019).toList()[0].amount;
    }
  }

  createPie(int shopping, int food, int house, int education, int travel, int personal, int investment){
    return <CircularStackEntry>[
      new CircularStackEntry(
        <CircularSegmentEntry>[
          new CircularSegmentEntry(0.0+education, Colors.red[200], rankKey: 'Q1'),
          new CircularSegmentEntry(0.0+food, Colors.green[200], rankKey: 'Q2'),
          new CircularSegmentEntry(0.0+house, Colors.blue[200], rankKey: 'Q3'),
          new CircularSegmentEntry(0.0+investment, Colors.yellow[200], rankKey: 'Q4'),
          new CircularSegmentEntry(0.0+personal, Colors.deepOrange[200], rankKey: 'Q5'),
          new CircularSegmentEntry(0.0+shopping, Colors.brown[200], rankKey: 'Q6'),
          new CircularSegmentEntry(0.0+travel, Colors.blueGrey[200], rankKey: 'Q7'),
        ],
        rankKey: 'Quarterly Profits',
      ),
    ];
  }




}
