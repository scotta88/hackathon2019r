import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:financial_assistant/SplashPageView.dart';
import 'LoginPage.dart';
import 'DashboardPage.dart';

class SplashPage extends StatefulWidget {
  static String tag = 'splash-page';

  @override
  SplashPageView createState() => new SplashPageView();
}

abstract class SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
    countDownTime();
  }

  final int splashDuration = 2;

  countDownTime() async {
    return Timer(Duration(seconds: splashDuration), () {
      SystemChannels.textInput.invokeMethod('TextInput.hide');
      Navigator.of(context).pushReplacementNamed(LoginPage.tag);
    });
  }
}
