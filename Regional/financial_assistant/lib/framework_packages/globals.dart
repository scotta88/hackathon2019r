library my_prj.globals;
import 'package:http/http.dart' as http;
import 'dart:convert';
import "package:collection/collection.dart";

List<Transaction> transactionsList = List();
var isDataLoading = false;
Duration _cacheValidDuration = Duration(minutes: 30);
DateTime _lastFetchTime = DateTime.fromMillisecondsSinceEpoch(0);
double monthlyIncome = 4000;

double avgMonthlySpendOnEducation;
double avgMonthlySpendOnFood;
double avgMonthlySpendOnHousehold;
double avgMonthlySpendOnInvestment;
double avgMonthlySpendOnPersonal;
double avgMonthlySpendOnShopping;
double avgMonthlySpendOnTravel;
double avgMonthlySavings;
double avgMonthlySpendings;

List<Transaction> nonEssentialRegularTransactionsList = List();

List<Transaction> upcomingPayments = List();
double avgMonthlySpendOnNR;

int upcomingElectricityBill;
int upcomingRentBill;

class Transaction implements Comparable<Transaction>{
  // ignore: non_constant_identifier_names
  final String date;
  final int month;
  final int year;
  final int account;
  final String item;
  final String operation;
  final int amount;
  final String category;
  final String subCategory;
  Transaction._({this.date, this.account,this.item, this.operation,this.amount, this.category, this.subCategory}): month = GetMonth(date), year = GetYear(date);
  factory Transaction.fromJson(Map<String, dynamic> json) {

    return new Transaction._(
      date: json['tran_date'],
      account: json['account_no'],
      item: json['item'],
      operation: json['operation'],
      amount: json['amount'],
      category: json['category'],
      subCategory: json['subCategory'],
    );
  }

  @override
  int compareTo(Transaction other) {
    var otherDateArray = other.date.split("/");
    var myDateArray = this.date.split("/");
      DateTime otherDate = DateTime(int.parse(otherDateArray[2]),int.parse(otherDateArray[1]),int.parse(otherDateArray[0]));
    DateTime myDate = DateTime(int.parse(myDateArray[2]),int.parse(myDateArray[1]),int.parse(myDateArray[0]));
    int order = myDate.compareTo(otherDate) * (-1);
    return order;
  }
}
GetMonth(String date){
  return int.parse(date.split("/")[1]);
}

GetYear(String date){
  return int.parse(date.split("/")[2]);
}
refreshTransactionsData() async {
  isDataLoading = true;
  var response = await http.get(
    "https://hackaholics-bfdcb.firebaseio.com/transactions.json",
    headers: {"Accept": "application/json"},
  );
  if (response.statusCode == 200) {
    transactionsList = (json.decode(response.body) as List)
        .map((data) => new Transaction.fromJson(data))
        .toList();
    isDataLoading = false;
  } else {
    print('Something went wrong. \nResponse Code : ${response.statusCode}');
  }
}

getAllTransactions({bool forceRefresh = false}) async {
  bool shouldRefreshFromApi = (null == transactionsList || transactionsList.isEmpty ||
      null == _lastFetchTime ||
      _lastFetchTime.isBefore(DateTime.now().subtract(_cacheValidDuration)) ||
      forceRefresh);

  if (shouldRefreshFromApi)
    await refreshTransactionsData();
}

getAllCategoriesAvgSpend(){
  avgMonthlySpendOnEducation = calculateAvgMonthlySpendOnCategory("education");
  avgMonthlySpendOnFood = calculateAvgMonthlySpendOnCategory("food/liquor");
  avgMonthlySpendOnHousehold = calculateAvgMonthlySpendOnCategory("household");
  avgMonthlySpendOnInvestment = calculateAvgMonthlySpendOnCategory("investment");
  avgMonthlySpendOnPersonal = calculateAvgMonthlySpendOnCategory("personal_care");
  avgMonthlySpendOnShopping = calculateAvgMonthlySpendOnCategory("shopping");
  avgMonthlySpendOnTravel = calculateAvgMonthlySpendOnCategory("travel");
  avgMonthlySpendings = avgMonthlySpendOnEducation + avgMonthlySpendOnFood + avgMonthlySpendOnHousehold + avgMonthlySpendOnInvestment + avgMonthlySpendOnPersonal +avgMonthlySpendOnShopping + avgMonthlySpendOnTravel;
  avgMonthlySavings = monthlyIncome - avgMonthlySpendings;
}

getAllSubCategoriesAvgSpend(){
  avgMonthlySpendOnNR = calculateAvgMonthlySpendOnSubCategory("NR");
}

double calculateAvgMonthlySpendOnCategory(String category){
  var categorizedTransactionsList = new List<Transaction>();
  categorizedTransactionsList = transactionsList.where((i) => i.category == category).toList();
  var newMap  = groupBy(categorizedTransactionsList, (obj) => obj.month);

  int totalMonthlySpend = 0;
  for(var key in newMap.keys){
    totalMonthlySpend = totalMonthlySpend + newMap[key].map((m) => m.amount).reduce((a, b) => a + b);
  }
  double avgMonthlySpend = totalMonthlySpend/newMap.keys.length;
  return avgMonthlySpend;
}

double calculateAvgMonthlySpendOnSubCategory(String category){
  var categorizedTransactionsList = new List<Transaction>();
  categorizedTransactionsList = transactionsList.where((i) => i.subCategory == category).toList();
  var newMap  = groupBy(categorizedTransactionsList, (obj) => obj.month);

  int totalMonthlySpend = 0;
  for(var key in newMap.keys){
    totalMonthlySpend = totalMonthlySpend + newMap[key].map((m) => m.amount).reduce((a, b) => a + b);
    nonEssentialRegularTransactionsList = newMap[key].toList();
  }
  double avgMonthlySpend = totalMonthlySpend/newMap.keys.length;
  return avgMonthlySpend;
}