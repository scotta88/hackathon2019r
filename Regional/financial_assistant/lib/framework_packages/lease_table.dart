import 'package:flutter/material.dart';
import '../ContractDetailsPage.dart';
import '../ContractLeasePage.dart';

class LeaseTable extends StatefulWidget {
  
  bool leaseTableType;

LeaseTable(this.leaseTableType);

  @override
  _LeaseTableState createState() => _LeaseTableState();
}

class _LeaseTableState extends State<LeaseTable> {
  @override
  Widget build(BuildContext context) {
    List<LeaseInfo> items = new List<LeaseInfo>(); 
    
     
    if (widget.leaseTableType==true){
     items = leaseItems;
 
    }
    else{
      items = loanItems;
 
    }

    return DataTable(
      onSelectAll: (b) {},
      sortAscending: true,
      columns: <DataColumn>[
        DataColumn(
          label: Row(
            children: <Widget>[
              Container(child: Icon(Icons.control_point)),
              Container(
                  margin: EdgeInsets.only(left: 5),
                  child: Text(
                    'CONTRACTS:',
                    textScaleFactor: 1.5,
                  ))
            ],
          ),
        ),
      ],
      rows: items
          .map(
            (itemRow) => DataRow(
                  cells: [
                    DataCell(
                      Row(
                        children: <Widget>[
                          Text(itemRow.leaseName),
                          Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(itemRow.leaseReference,
                                  textScaleFactor: 1.4))
                        ],
                      ),
                      showEditIcon: false,
                      placeholder: false,
                      onTap: (){  
                        if(widget.leaseTableType==true){
                        Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ContractLeasePage()));}
                      else{
                          Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ContractDetailsPage(itemRow.leaseReference)));
                      }
                      
                      }
                    ),
                  ],
                ),
          )
          .toList(),
    );
  }
}

class LeaseInfo {
  String leaseName;
  String leaseReference;

  LeaseInfo({
    this.leaseName,
    this.leaseReference,
  });
}

List leaseItems = <LeaseInfo>[
  LeaseInfo(
    leaseName: '1',
    leaseReference: 'REFERENCE NUMBER#1',
  ),
   LeaseInfo(
    leaseName: '2',
    leaseReference: 'REFERENCE NUMBER#2',
  ),
];

List loanItems = <LeaseInfo>[
  LeaseInfo(
    leaseName: '1',
    leaseReference: 'LOAN CONTRACT #1',
  ),
   LeaseInfo(
    leaseName: '2',
    leaseReference: 'LOAN CONTRACT #2',
  ),
];