import 'dart:async';
import 'dart:convert';
import 'dart:io';


main() async{
  String url = 'https://ussouthcentral.services.azureml.net/workspaces/87ba8e0bba7a4e7faac9201f1d29bf1f/services/68c5453efde84692931a4679a88c8461/execute?api-version=2.0&details=true';
  Map map = {
    "Inputs": {
      "input1": {
        "ColumnNames": [
          "tran_id",
          "tran_date",
          "account_no",
          "item/store",
          "operation",
          "amount",
          "category"
        ],
        "Values": [
          [
            "",
            "",
            "",
            "bus",
            "",
            "",
            ""
          ]
        ]
      }
    },
    "GlobalParameters": {}
  }
  ;

  print(await apiRequest(url, map));
}

Future<String> apiRequest(String url, Map jsonMap) async{
  HttpClient httpClient = new HttpClient();
  HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
  request.headers.set('Content-Type', 'application/json');
  request.headers.set('Authorization', 'Bearer jj+2FZTtMzNzgUhsyU9Pv52Rf8haF14gs3cANtIYS0nSqcUkxh2/Reo5WZ9C7zzJbvmGUJwwjw6fbYXWFi1dng==');
//  request.headers.set('Content-Length', '100000');
  request.headers.set('Accept', 'application/json');
  request.add(utf8.encode(json.encode(jsonMap)));
  HttpClientResponse response = await request.close();
  String reply = await response.transform(utf8.decoder).join();
  httpClient.close();
  return reply;
}


