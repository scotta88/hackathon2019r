import 'package:flutter/material.dart';

class BudgetingTable extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _BudgetingTableState();
  }
}

class _BudgetingTableState extends State<BudgetingTable> {
  void _getSelectedRowInfo() {
    print('Selected Item Row Name Here...');
  }
TextEditingController myController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return DataTable(
      onSelectAll: (b) {},
      sortAscending: true,
      columns: <DataColumn>[
        DataColumn(
          label: Row(
            children: <Widget>[
              Container(child: Icon(Icons.category,color: Colors.deepPurpleAccent)),
              Container(
                  margin: EdgeInsets.only(left: 5),
                  child: Text(
                    'CATEGORY',
                    textScaleFactor: 1.5,
                  ))
            ],
          ),
        ),
        DataColumn(
        
          label: Row(
            children: <Widget>[
              Container(child: Icon(Icons.monetization_on,color: Colors.deepPurpleAccent,)),
              Container(
                  margin: EdgeInsets.only(left: 5),
                  child: Text(
                    'AMOUNT',
                    textScaleFactor: 1.5,
                  ))
            ],
          ),
        ),
      ],
      rows: items
          .map(
            (itemRow) => DataRow(
                  cells: [
                    DataCell(
                      Row(
                        children: <Widget>[
                          itemRow.itemIcon,
                          Container(
                              margin: EdgeInsets.only(left: 10),
                              child:
                                  Text(itemRow.itemName, textScaleFactor: 1.4))
                        ],
                      ),
                      showEditIcon: false,
                      placeholder: false,
                    ),
                    DataCell(
                      TextField(
                        style: itemRow.itemPressed ? TextStyle(color: Colors.red) : TextStyle(color: Colors.green ),
                        onChanged: (v) {myController.text = v;
                        setState(() {
                          itemRow.itemPressed = int.parse(v)> itemRow.avePrice ? true: false;
                        });
                        },
                          keyboardType: TextInputType.number,
                          textAlign: TextAlign.left,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: itemRow.itemPrice.toString(),
//                            hintStyle: pressed ? TextStyle(color: Colors.red) : TextStyle(color: Colors.green ),
                          ),
                      ),
                      showEditIcon: false,
                      placeholder: false,
//                      onTap: _getSelectedRowInfo,
                    ),
                  ],
                ),
          )
          .toList(),
    );
  }

  changeColorState(ItemInfo itemRow) {

  }
}

class ItemInfo {
  String itemName;
  int itemPrice;
  Color itemColor;
  bool itemPressed;
  int avePrice;
  Icon itemIcon;

  ItemInfo({
    this.itemName,
    this.itemPrice,
    this.itemColor,
    this.itemPressed,
    this.avePrice,
    this.itemIcon
  });
}

var items = <ItemInfo>[
  ItemInfo(
    itemName: 'Shopping',
    itemPrice: 0,
    itemColor: Colors.green,
    itemPressed: false,
    avePrice: 813,
    itemIcon: Icon(Icons.add_shopping_cart)
  ),
  ItemInfo(
    itemName: 'Food',
    itemPrice: 0,
    itemColor: Colors.green,
      itemPressed: false,
      avePrice: 1776,
itemIcon: Icon(Icons.fastfood)
  ),
  ItemInfo(
    itemName: 'Household',
    itemPrice: 0,
    itemColor: Colors.green,
      itemPressed: false,
      avePrice: 2118,
itemIcon: Icon(Icons.local_grocery_store)
  ),
  ItemInfo(
    itemName: 'Education',
    itemPrice: 0,
    itemColor: Colors.green,
      itemPressed: false,
      avePrice: 1928,
itemIcon: Icon(Icons.school)
  ),
  ItemInfo(
    itemName: 'Personal Care',
    itemPrice: 0,
    itemColor: Colors.green,
      itemPressed: false,
      avePrice: 364,
      itemIcon: Icon(Icons.local_hospital)
  ),
  ItemInfo(
      itemName: 'Travel',
      itemPrice: 0,
      itemColor: Colors.green,
      itemPressed: false,
      avePrice: 848,
      itemIcon: Icon(Icons.flight)
  ),
  ItemInfo(
      itemName: 'Investment',
      itemPrice: 0,
      itemColor: Colors.green,
      itemPressed: false,
      avePrice: 350,
      itemIcon: Icon(Icons.attach_money)
  ),
];
