import 'package:flutter/material.dart';
import 'package:financial_assistant/NavigationDrawer.dart';
import 'DashboardPage.dart';
import 'DetailsPage.dart';
import 'LoginPage.dart';
import 'SpendingForecastPage.dart';
import 'AssistantPage.dart';
import 'ContractsSummaryPage.dart';
import 'BudgetingPage.dart';
import 'ContractSummary.dart';
import 'RecommendationPage.dart';

class NavigationDrawerView extends NavigationDrawerState {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
        AppBar(
            backgroundColor: Colors.indigoAccent,
            automaticallyImplyLeading: false,
            centerTitle: true,
            title: Text("FINANCIAL ASSISTANT", style: TextStyle(color: Colors.white)),
          ),
          Container(
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.person,
                  size: 80,
                  color: Colors.blueGrey,
                ),
                Column(
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(),
                        child: Text("Bob Aranofsky", textScaleFactor: 1.5)),
                    Text("bobdarmeinlauf@gmail.com")
                  ],
                ),
              ],
            ),
          ),
          Container(
              // color: Colors.grey[300],
              margin: EdgeInsets.only(top: 29),
              decoration: BoxDecoration(
                  border: Border(
                      top: BorderSide(color: Theme.of(context).dividerColor))),
              child: ListTile(
                leading: Icon(Icons.account_balance),
                title: Text("SUMMARY"),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => DashboardPage()));
                },
              )),
          ListTile(
            leading: Icon(Icons.mic),
            title: Text("ASSISTANT"),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => AssistantPage()));
            },
          ),
          ListTile(
            leading: Icon(Icons.access_time),
            title: Text("SPENDING FORECAST"),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => SpendingForecastPage()));
            },
          ),
          ListTile(
            leading: Icon(Icons.folder_open),
            title: Text("TRANSACTION DETAILS"),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => DetailsPage()));
            },
          ),
          ListTile(
            leading: Icon(Icons.account_balance_wallet),
            title: Text("BUDGETING"),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => RecommendationPage()));
            },
          ),
          ListTile(
            leading: Icon(Icons.note),
            title: Text("CONTRACTS"),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ContractSummary()));
            },
          ),
          Container(
              // color: Colors.grey[300],
              margin: EdgeInsets.only(top: 29),
              decoration: BoxDecoration(
                  border: Border(
                      top: BorderSide(color: Theme.of(context).dividerColor))),
              child: Center(
                  child: ListTile(
                leading: Icon(Icons.exit_to_app),
                title: Text(
                  "LOGOUT",
                  textScaleFactor: 1.1,
                ),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => null));
                },
              )))
        ],
      ),
    );
  }
}
