import 'package:flutter/material.dart';
import 'package:financial_assistant/DetailsPageView.dart';
import 'framework_packages/globals.dart';
//import 'package:http/http.dart' as http;
//import 'dart:convert';

class DetailsPage extends StatefulWidget {
  static String tag = 'details-page';

  @override
  DetailsPageView createState() => new DetailsPageView();
}

class Transaction {
  // ignore: non_constant_identifier_names
  final String date;
  final int account;
  final String item;
  final String operation;
  final int amount;
  final String category;
  Transaction._(
      {this.date,
      this.account,
      this.item,
      this.operation,
      this.amount,
      this.category});
  factory Transaction.fromJson(Map<String, dynamic> json) {
    return new Transaction._(
      date: json['tran_date'],
      account: json['account_no'],
      item: json['item'],
      operation: json['operation'],
      amount: json['amount'],
      category: json['category'],
    );
  }
}

abstract class DetailsPageState extends State<DetailsPage> {
  var list = List();
  var isLoading = false;
  DateTime fromDate = DateTime.now();
  DateTime toDate = DateTime.now();
  TextEditingController fromController = new TextEditingController();
  TextEditingController toController = new TextEditingController();


  getTranscations(){
    getAllTransactions();
    list = transactionsList;
    list.sort();
  }

  Map<String, String>categoryMap = {
    "All" : "All",
    "Shopping" : "shopping",
    "Food Liquor"  : "food/liquor",
    "Household" : "household",
    "Education": "education",
    "Personal Care": "personal_care",
    "Travel": "travel",
    "Investment":"investment"
  };


    getDataByDateAndCategory(String from, String to, String category){
      category = categoryMap[category];
      list.clear();
      getAllTransactions();
      for (var transaction in transactionsList){
        if(category == 'All' || transaction.category == category){
          if(isLargerOrEqual(transaction.date, from) && isLargerOrEqual(to, transaction.date)){
            list.add(transaction);
          }
        }
      }
      list.sort();
      return list;
    }

    isLargerOrEqual(String a, String b){
      var a_array = a.split("/");
      var b_array = b.split("/");
      if(int.parse(a_array[2]) > int.parse(b_array[2])){
        return true;
      }else if(int.parse(a_array[2]) < int.parse(b_array[2])){
        return false;
      }else{
        if(int.parse(a_array[1]) > int.parse(b_array[1])){
          return true;
        }
        else if(int.parse(a_array[1]) < int.parse(b_array[1])){
          return false;
        }else{
          if(int.parse(a_array[0]) >= int.parse(b_array[0])){
            return true;
          }else{
            return false;
          }
        }
      }
    }



}
