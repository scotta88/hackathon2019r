import 'package:flutter/material.dart';
import 'package:financial_assistant/RegisterPageView.dart';

class RegisterPage extends StatefulWidget {
  static String tag = 'register-page';

  @override
  RegisterPageView createState() => new RegisterPageView();
}

abstract class RegisterPageState extends State<RegisterPage> {}
