import 'package:flutter/material.dart';
import 'package:financial_assistant/LoginPage.dart';
import 'package:flutter_auth_buttons/flutter_auth_buttons.dart';
import 'LoginEmailPage.dart';
import 'RegisterPage.dart';
import 'DashboardPage.dart';

class LoginPageView extends LoginPageState {
  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset('assets/logo2.png'),
      ),
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 4.0),
      child: RaisedButton(
        shape: BeveledRectangleBorder(
          borderRadius: BorderRadius.circular(5),
        ),
        onPressed: () => Navigator.of(context).pushNamed(RegisterPage.tag),
        padding: EdgeInsets.all(2),
        color: Colors.grey[200],
        child: Text('Email', style: TextStyle(color: Colors.black87, fontSize: 20)),
      ),
    );

    final registerButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 4.0),
      child: RaisedButton(
        shape: BeveledRectangleBorder(
          borderRadius: BorderRadius.circular(5),
        ),
        onPressed: () => Navigator.of(context).pushNamed(RegisterPage.tag),
        padding: EdgeInsets.all(2),
        color: Colors.grey[200],
        child: Text('Register', style: TextStyle(color: Colors.black87, fontSize: 20)),
      ),
    );

//    final debugSkipButton = Padding(
//      padding: EdgeInsets.symmetric(vertical: 4.0),
//      child: RaisedButton(
//        shape: BeveledRectangleBorder(
//          borderRadius: BorderRadius.circular(5),
//        ),
//        onPressed: () =>
//            Navigator.of(context).pushReplacementNamed(DashboardPage.tag),
//        padding: EdgeInsets.all(12),
//        color: Colors.white,
//        child: Text('Debug Skip', style: TextStyle(color: Colors.black54)),
//      ),
//    );

    return Scaffold(
      backgroundColor: Colors.blueAccent[700],
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            logo,
            SizedBox(height: 25.0),
            GoogleSignInButton(onPressed: () => handleSignIn(context)),
            loginButton,
            registerButton,
            //debugSkipButton
          ],
        ),
      ),
    );
  }
}
