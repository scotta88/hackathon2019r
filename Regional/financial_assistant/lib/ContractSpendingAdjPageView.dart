import 'package:flutter/material.dart';
import 'package:financial_assistant/ContractSpendingAdjPage.dart';
import 'NavigationDrawer.dart';

class ContractSpendingAdjPageView extends ContractSpendingAdjPageState {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Spending Adjustments',
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            backgroundColor: Colors.blue[100],
            drawer: NavigationDrawer(),
            appBar: AppBar(
              title: Text("SPENDING ADJUSTMENTS"),
              backgroundColor: Colors.indigoAccent,
              centerTitle: true,
            ),
            body: Column(children: <Widget>[])));
  }
}
