import 'package:flutter/material.dart';
import 'package:financial_assistant/ContractsSummaryPage.dart';
import 'NavigationDrawer.dart';

class ContractsSummaryPageView extends ContractsSummaryPageState {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Contract Summary',
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            backgroundColor: Colors.blue[100],
            drawer: NavigationDrawer(),
            appBar: AppBar(
              title: Text("CONTRACT SUMMARY"),
              backgroundColor: Colors.indigoAccent,
              centerTitle: true,
            ),
            body: Column(children: <Widget>[])));
  }
}


