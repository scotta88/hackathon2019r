import 'package:flutter/material.dart';
import 'package:flutter_dialogflow/dialogflow_v2.dart';
import 'package:tts/tts.dart';
import 'framework_packages/speech_recognition.dart';
import 'package:financial_assistant/framework_packages/ChatMessage.dart';
import 'package:financial_assistant/AssistantPageView.dart';
//import 'package:simple_permissions/simple_permissions.dart';

class AssistantPage extends StatefulWidget {
  static String tag = 'assistant-page';

  @override
  AssistantPageView createState() => new AssistantPageView();
}

const languages = const [const ChatLanguage('English', 'en_US')];

class ChatLanguage {
  final String name;
  final String code;

  const ChatLanguage(this.name, this.code);
}

abstract class AssistantPageState extends State<AssistantPage> {
  ChatLanguage selectedLang = languages.first;
  final List<ChatMessage> messages = <ChatMessage>[];
  final TextEditingController textController = new TextEditingController();
  SpeechRecognition _speech;

  bool speechRecognitionAvailable = false;
  bool isListening = false;

  @override
  initState() {
    super.initState();
    // _checkAudioPermission();
    activateSpeechRecognizer();
  }

  /* void _checkAudioPermission() async {
    bool hasPermission =
        await SimplePermissions.checkPermission(Permission.RecordAudio);
    if (!hasPermission) {
      await SimplePermissions.requestPermission(Permission.RecordAudio);
    }
  }*/

  void response(query) async {
    textController.clear();
    if (query == null) return;
    AuthGoogle authGoogle = await AuthGoogle(
            fileJson: "assets/financial-assistant-credentials.json")
        .build();
    // Select Language.ENGLISH or Language.SPANISH or others...
    Dialogflow dialogflow =
        Dialogflow(authGoogle: authGoogle, language: Language.english);
    AIResponse response = await dialogflow.detectIntent(query);
    ChatMessage message = new ChatMessage(
      text: response.getMessage(),
      name: "Alf the Bot",
      type: false,
    );
    Tts.speak(response.getMessage());
    setState(() {
      messages.insert(0, message);
    });
  }

  void handleSubmitted(String text) {
    textController.clear();
    ChatMessage message = new ChatMessage(
      text: text,
      name: "Me",
      type: true,
    );
    setState(() {
      messages.insert(0, message);
    });
    response(text);
  }

  void activateSpeechRecognizer() {
    //if(!checkPermission()){
    //  requestPermission();
    //}
    print('_MyAppState.activateSpeechRecognizer... ');
    _speech = new SpeechRecognition();
    _speech.setAvailabilityHandler(onSpeechAvailability);
    _speech.setCurrentLocaleHandler(onCurrentLocale);
    _speech.setRecognitionStartedHandler(onRecognitionStarted);
    _speech.setRecognitionResultHandler(onRecognitionResult);
    _speech.setRecognitionCompleteHandler(onRecognitionComplete);
    _speech
        .activate()
        .then((res) => setState(() => speechRecognitionAvailable = res));
  }

  void handleSpeechPressed() {
    if (speechRecognitionAvailable) {
      if (isListening) {
        stop();
      } else {
        start();
      }
    }
  }

  void start() => _speech
      .listen(locale: selectedLang.code)
      .then((result) => print('_MyAppState.start => result $result'));

  void cancel() =>
      _speech.cancel().then((result) => setState(() => isListening = result));

  void stop() => _speech.stop().then((result) {
        setState(() => isListening = result);
      });

  void onSpeechAvailability(bool result) =>
      setState(() => speechRecognitionAvailable = result);

  void onCurrentLocale(String locale) {
    print('_MyAppState.onCurrentLocale... $locale');
    setState(
        () => selectedLang = languages.firstWhere((l) => l.code == locale));
  }

  void onRecognitionStarted() => setState(() => isListening = true);

  void onRecognitionResult(String text) => {};

  void onRecognitionComplete(String text) => {
        isListening ? handleSubmitted(text) : null,
        setState(() => {isListening = false}),
      };

  void errorHandler() => print("Error"); //activateSpeechRecognizer();
}
