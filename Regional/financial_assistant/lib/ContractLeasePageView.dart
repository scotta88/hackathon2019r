import 'package:flutter/material.dart';
import 'package:financial_assistant/ContractLeasePage.dart';
import 'NavigationDrawer.dart';

class ContractLeasePageView extends ContractLeasePageState {
  @override
  Widget build(BuildContext context) {
    int outstandingPrincipal = 1000;
    int outstandingInterest = 100;
    int totalOutstandingAmount = 1100;
    String maturityDate = "20/04/19";
    int numberOfAssets = 1;
    String assetName = "Aircraft";
    int currentMonthlyPayment = 300;
    int averageMonthlyAvailableFunds = 430;
    int potentialMaxFunds = 500;

    return MaterialApp(
        title: 'Lease Details',
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            backgroundColor: Colors.teal[50],
            drawer: NavigationDrawer(),
            appBar: AppBar(
              iconTheme: IconThemeData(color: Colors.white),
              title: Text("LEASE DETAILS",style: TextStyle(color: Colors.white),),
              backgroundColor: Colors.indigoAccent,
              centerTitle: true,
            ),
            body: ListView(
              children: <Widget>[
                Column(children: <Widget>[
                  Container(
                    width: 400,
                    height: 300,
                    padding: const EdgeInsets.only(top:20.0,left:20.0,right:20.0),
                    child: new Material(
                      animationDuration: new Duration(milliseconds: 500),
                      elevation: 2.0,
                      borderRadius: new BorderRadius.all(Radius.circular(5.0)),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          new Transform(
                            transform:
                                new Matrix4.translationValues(9, 0.0, 0.0),
                            child: Container(
                              
                              width: 300,
                              decoration: BoxDecoration(
                                  border: new Border(
                                      bottom: BorderSide(color: Colors.grey))),
                              child: Center(
                                child: Container(
                                    margin: EdgeInsets.only(left:8,right:8,bottom:8),
                      
                                    child: Text("ASSET DETAILS:",
                                        style: new TextStyle(
                                            fontSize: 23.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black))),
                              ),
                            ),
                            // child: new Text(
                            //   "CURRENT STATUS:",
                            //   style: new TextStyle(
                            //       fontSize: 20.0,
                            //       fontWeight: FontWeight.bold,
                            //       color: Colors.black),
                            // ),
                          ),
                          new Transform(
                            transform:
                                new Matrix4.translationValues(10, 0.0, 0.0),
                            child: Column(
                              children: <Widget>[
                                Row(children: <Widget>[
                                  Container(
                                      margin: EdgeInsets.only(left: 6),
                                      child: Text("Asset name:",
                                          softWrap: true,
                                          textAlign: TextAlign.center,
                                          style: new TextStyle(
                                              fontWeight: FontWeight.normal,
                                              fontSize: 17.0,
                                              color: Colors.black))),
                                  Container(
                                      margin: EdgeInsets.only(left: 120),
                                      child: Text("${assetName}",
                                          softWrap: true,
                                          textAlign: TextAlign.center,
                                          style: new TextStyle(
                                              fontWeight: FontWeight.normal,
                                              fontSize: 17.0,
                                              color: Colors.black))),
                                ]),
                                Row(children: <Widget>[
                                  Container(
                                      margin: EdgeInsets.only(left: 6),
                                      child: Text("Numbers of assets:",
                                          softWrap: true,
                                          textAlign: TextAlign.center,
                                          style: new TextStyle(
                                              fontWeight: FontWeight.normal,
                                              fontSize: 17.0,
                                              color: Colors.black))),
                                  Container(
                                      margin: EdgeInsets.only(left: 110),
                                      child: Text("\ ${numberOfAssets}",
                                          softWrap: true,
                                          textAlign: TextAlign.center,
                                          style: new TextStyle(
                                              fontWeight: FontWeight.normal,
                                              fontSize: 17.0,
                                              color: Colors.black))),
                                ]),
                              ],
                            ),

                            //  new Text("widget.content",
                            //     softWrap: true,
                            //     textAlign: TextAlign.center,
                            //     style: new TextStyle(
                            //         fontWeight: FontWeight.normal,
                            //         fontSize: 15.0,
                            //         color: Colors.black)),
                          ),
                          new Icon(
                            Icons.assignment,
                            size: 100.0,
                            color: Colors.green,
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: 400,
                    height: 300,
                    padding: const EdgeInsets.all(20.0),
                    child: new Material(
                      animationDuration: new Duration(milliseconds: 500),
                      elevation: 2.0,
                      borderRadius: new BorderRadius.all(Radius.circular(5.0)),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          new Transform(
                            transform:
                                new Matrix4.translationValues(10, 0.0, 0.0),
                            child: Container(
                              width: 300,
                              decoration: BoxDecoration(
                                  border: new Border(
                                      bottom: BorderSide(color: Colors.grey))),
                              child: Center(
                                child: Container(
                                    margin: EdgeInsets.all(8),
                                    child: Text("CURRENT STATUS",
                                        style: new TextStyle(
                                            fontSize: 23.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black))),
                              ),
                            ),
                            // child: new Text(
                            //   "CURRENT STATUS:",
                            //   style: new TextStyle(
                            //       fontSize: 20.0,
                            //       fontWeight: FontWeight.bold,
                            //       color: Colors.black),
                            // ),
                          ),
                          new Transform(
                            transform:
                                new Matrix4.translationValues(10, 0.0, 0.0),
                            child: Column(
                              children: <Widget>[
                                Row(children: <Widget>[
                                  Container(
                                      padding: EdgeInsets.all(1),
                                      margin: EdgeInsets.only(left: 6),
                                      child: Text("Outstanding principal:",
                                          softWrap: true,
                                          textAlign: TextAlign.center,
                                          style: new TextStyle(
                                              fontWeight: FontWeight.normal,
                                              fontSize: 17.0,
                                              color: Colors.black))),
                                  Container(
                                      padding: EdgeInsets.all(1),
                                      margin: EdgeInsets.only(left: 80),
                                      child: Text("\$ ${outstandingPrincipal}",
                                          softWrap: true,
                                          textAlign: TextAlign.center,
                                          style: new TextStyle(
                                              fontWeight: FontWeight.normal,
                                              fontSize: 17.0,
                                              color: Colors.black))),
                                ]),
                                Row(children: <Widget>[
                                  Container(
                                      padding: EdgeInsets.all(1),
                                      margin: EdgeInsets.only(left: 6),
                                      child: Text("Outstanding interest:",
                                          softWrap: true,
                                          textAlign: TextAlign.center,
                                          style: new TextStyle(
                                              fontWeight: FontWeight.normal,
                                              fontSize: 17.0,
                                              color: Colors.black))),
                                  Container(
                                      padding: EdgeInsets.all(1),
                                      margin: EdgeInsets.only(left: 89),
                                      child: Text("\$ ${outstandingInterest}",
                                          softWrap: true,
                                          textAlign: TextAlign.center,
                                          style: new TextStyle(
                                              fontWeight: FontWeight.normal,
                                              fontSize: 17.0,
                                              color: Colors.black))),
                                ]),
                                Row(children: <Widget>[
                                  Container(
                                      padding: EdgeInsets.all(1),
                                      margin: EdgeInsets.only(left: 6),
                                      child: Text("Total outstanding amt:",
                                          softWrap: true,
                                          textAlign: TextAlign.center,
                                          style: new TextStyle(
                                              fontWeight: FontWeight.normal,
                                              fontSize: 17.0,
                                              color: Colors.black))),
                                  Container(
                                      padding: EdgeInsets.all(1),
                                      margin: EdgeInsets.only(left: 76),
                                      child: Text(
                                          "\$ ${totalOutstandingAmount}",
                                          softWrap: true,
                                          textAlign: TextAlign.center,
                                          style: new TextStyle(
                                              fontWeight: FontWeight.normal,
                                              fontSize: 17.0,
                                              color: Colors.black))),
                                ]),
                                Row(children: <Widget>[
                                  Container(
                                      padding: EdgeInsets.all(1),
                                      margin:
                                          EdgeInsets.only(left: 6, bottom: 0),
                                      child: Text("Maturity date:",
                                          softWrap: true,
                                          textAlign: TextAlign.center,
                                          style: new TextStyle(
                                              fontWeight: FontWeight.normal,
                                              fontSize: 17.0,
                                              color: Colors.black))),
                                  Container(
                                      padding: EdgeInsets.all(1),
                                      margin:
                                          EdgeInsets.only(left: 125, bottom: 0),
                                      child: Text("${maturityDate}",
                                          softWrap: true,
                                          textAlign: TextAlign.center,
                                          style: new TextStyle(
                                              fontWeight: FontWeight.normal,
                                              fontSize: 17.0,
                                              color: Colors.black))),
                                ]),
                              ],
                            ),

                            //  new Text("widget.content",
                            //     softWrap: true,
                            //     textAlign: TextAlign.center,
                            //     style: new TextStyle(
                            //         fontWeight: FontWeight.normal,
                            //         fontSize: 15.0,
                            //         color: Colors.black)),
                          ),
                              Container(
                      margin: EdgeInsets.only(top: 0),
                      child: RaisedButton(
                        color: Colors.blue,
                        child: Text("RENEWAL RECOMMENDATION",
                            style: TextStyle(color: Colors.white)),
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              // return object of type Dialog
                              return AlertDialog(
                                title:
                                    Center(child: new Text("RECOMMENDATION")),
                                content: Container(
                                    height: 70,
                                    child: Column(
                                      children: <Widget>[
                                        Text(
                                            "Current monthly payment: ${currentMonthlyPayment}"),
                                        Text(
                                            "Average monthly available funds: ${averageMonthlyAvailableFunds}"),
                                        Text(
                                            "Potential max funds: ${potentialMaxFunds}"),
                                      ],
                                    )),
                                actions: <Widget>[
                                  // usually buttons at the bottom of the dialog
                                  new FlatButton(
                                    child: new Text("Close"),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ],
                              );
                            },
                          );
                        },
                      )),
                          // new Icon(
                          //   Icons.account_balance,
                          //   size: 100.0,
                          //   color: Colors.green,
                          // )
                        ],
                      ),
                    ),
                  ),
              
                ])
              ],
            )));
  }
}
