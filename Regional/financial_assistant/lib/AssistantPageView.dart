import 'package:flutter/material.dart';
import 'package:financial_assistant/AssistantPage.dart';
import 'NavigationDrawer.dart';

class AssistantPageView extends AssistantPageState {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Assistant App',
        home: Scaffold(
            backgroundColor: Colors.blue[100],
            drawer: NavigationDrawer(),
            appBar: AppBar(
              title: Text("ASSISTANT"),
              backgroundColor: Colors.indigoAccent,
              centerTitle: true,
            ),
            body: new Column(children: <Widget>[
              new Flexible(
                  child: new ListView.builder(
                padding: new EdgeInsets.all(8.0),
                reverse: true,
                itemBuilder: (_, int index) => messages[index],
                itemCount: messages.length,
              )),
              new Divider(height: 1.0),
              new Container(
                decoration:
                    new BoxDecoration(color: Theme.of(context).cardColor),
                child: _buildTextComposer(),
              ),
            ])));
  }

  Widget _buildTextComposer() {
    return new IconTheme(
      data: new IconThemeData(color: Theme.of(context).accentColor),
      child: new Container(
        margin: const EdgeInsets.symmetric(horizontal: 8.0),
        child: new Row(
          children: <Widget>[
            new Flexible(
              child: new TextField(
                controller: textController,
                onSubmitted: handleSubmitted,
                decoration:
                    new InputDecoration.collapsed(hintText: "Send a message"),
              ),
            ),
            new Container(
              margin: new EdgeInsets.symmetric(horizontal: 4.0),
              child: new IconButton(
                  icon: new Icon(Icons.send),
                  onPressed: () => handleSubmitted(textController.text)),
            ),
            new Container(
              margin: new EdgeInsets.symmetric(horizontal: 4.0),
              child: new IconButton(
                  icon: speechRecognitionAvailable
                      ? isListening
                          ? new Icon(Icons.mic_off)
                          : new Icon(Icons.mic)
                      : new Icon(Icons.mic_none),
                  onPressed: () => handleSpeechPressed()),
            ),
          ],
        ),
      ),
    );
  }
}
