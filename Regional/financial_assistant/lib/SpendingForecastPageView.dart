import 'package:flutter/material.dart';
import 'package:financial_assistant/SpendingForecastPage.dart';
import 'package:flutter_circular_chart/flutter_circular_chart.dart';
import 'NavigationDrawer.dart';
import 'framework_packages/globals.dart' as globals;

class SpendingForecastPageView extends SpendingForecastPageState {
  @override
  initState() {
    super.initState();
    globals.getAllCategoriesAvgSpend();
    globals.getAllSubCategoriesAvgSpend();
    //cycleSamples();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavigationDrawer(),
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        title: Text("SPENDING FORECAST",style: TextStyle(color: Colors.white),),
        backgroundColor: Colors.indigoAccent,
        centerTitle: true,
      ),
      body: ListView(
        children: <Widget>[
          AnimatedCircularChart(
              key: chartKey,
              percentageValues: false,
              size: const Size(300, 300),
              initialChartData: initialData,
              chartType: CircularChartType.Radial,
              holeLabel: '\$'+(globals.monthlyIncome.toInt()).toString(),
              labelStyle: new TextStyle(
                color: Colors.blueGrey[600],
                fontWeight: FontWeight.bold,
                fontSize: 24.0,
              ),
          ),
          Row(
              children: <Widget>[
                Container(
                    margin: EdgeInsets.only(
                        left: 20, bottom: 10),
                    height: 20,
                    width: 20,
                    decoration: BoxDecoration(
                        borderRadius:
                        BorderRadius.circular(50),
                        color: Colors.green[200])),
                Container(
                    margin: EdgeInsets.only(
                        left: 10, bottom: 10),
                    child: Text(
                      "Education",
                      textScaleFactor: 1.3,
                    )),
                Container(
                    margin: EdgeInsets.only(
                        left: 10, bottom: 10),
                    child: Text('~'+' \$'+(globals.avgMonthlySpendOnEducation.toInt()).toString(),
                      textScaleFactor: 1.3,
                    ))
              ]),
          Row(children: <Widget>[
            Container(
                margin: EdgeInsets.only(
                    left: 20, bottom: 10),
                height: 20,
                width: 20,
                decoration: BoxDecoration(
                    borderRadius:
                    BorderRadius.circular(50),
                    color: Colors.red[200])),
            Container(
                margin: EdgeInsets.only(
                    left: 10, bottom: 10),
                child: Text(
                  "Food",
                  textScaleFactor: 1.3,
                )),
            Container(
                margin: EdgeInsets.only(
                    left: 10, bottom: 10),
                child: Text('~'+' \$'+(globals.avgMonthlySpendOnFood.toInt()).toString(),
                  textScaleFactor: 1.3,
                ))
          ]),
          Row(children: <Widget>[
            Container(
                margin: EdgeInsets.only(
                    left: 20, bottom: 10),
                height: 20,
                width: 20,
                decoration: BoxDecoration(
                    borderRadius:
                    BorderRadius.circular(50),
                    color: Colors.blue[200])),
            Container(
                margin: EdgeInsets.only(
                    left: 10, bottom: 10),
                child: Text(
                  "Household",
                  textScaleFactor: 1.3,
                )),
            Container(
                margin: EdgeInsets.only(
                    left: 10, bottom: 10),
                child: Text('~'+' \$'+(globals.avgMonthlySpendOnHousehold.toInt()).toString(),
                  textScaleFactor: 1.3,
                ))
          ]),
          Row(children: <Widget>[
            Container(
                margin: EdgeInsets.only(
                    left: 20, bottom: 10),
                height: 20,
                width: 20,
                decoration: BoxDecoration(
                    borderRadius:
                    BorderRadius.circular(50),
                    color: Colors.yellow[200])),
            Container(
                margin: EdgeInsets.only(
                    left: 10, bottom: 10),
                child: Text(
                  "investment",
                  textScaleFactor: 1.3,
                )),
            Container(
                margin: EdgeInsets.only(
                    left: 10, bottom: 10),
                child: Text('~'+' \$'+(globals.avgMonthlySpendOnInvestment.toInt()).toString(),
                  textScaleFactor: 1.3,
                ))
          ]),
          Row(children: <Widget>[
            Container(
                margin: EdgeInsets.only(
                    left: 20, bottom: 10),
                height: 20,
                width: 20,
                decoration: BoxDecoration(
                    borderRadius:
                    BorderRadius.circular(50),
                    color: Colors.deepOrange[200])),
            Container(
                margin: EdgeInsets.only(
                    left: 10, bottom: 10),
                child: Text(
                  "Personal",
                  textScaleFactor: 1.3,
                )),
            Container(
                margin: EdgeInsets.only(
                    left: 10, bottom: 10),
                child: Text('~'+' \$'+(globals.avgMonthlySpendOnPersonal.toInt()).toString(),
                  textScaleFactor: 1.3,
                ))
          ]),
          Row(children: <Widget>[
            Container(
                margin: EdgeInsets.only(
                    left: 20, bottom: 10),
                height: 20,
                width: 20,
                decoration: BoxDecoration(
                    borderRadius:
                    BorderRadius.circular(50),
                    color: Colors.brown[200])),
            Container(
                margin: EdgeInsets.only(
                    left: 10, bottom: 10),
                child: Text(
                  "Shopping",
                  textScaleFactor: 1.3,
                )),
            Container(
                margin: EdgeInsets.only(
                    left: 10, bottom: 10),
                child: Text('~'+' \$'+(globals.avgMonthlySpendOnShopping.toInt()).toString(),
                  textScaleFactor: 1.3,
                ))
          ]),
          Row(children: <Widget>[
            Container(
                margin: EdgeInsets.only(
                    left: 20, bottom: 10),
                height: 20,
                width: 20,
                decoration: BoxDecoration(
                    borderRadius:
                    BorderRadius.circular(50),
                    color: Colors.blueGrey[200])),
            Container(
                margin: EdgeInsets.only(
                    left: 10, bottom: 10),
                child: Text(
                  "Travel",
                  textScaleFactor: 1.3,
                )),
            Container(
                margin: EdgeInsets.only(
                    left: 10, bottom: 10),
                child: Text('~'+' \$'+(globals.avgMonthlySpendOnTravel.toInt()).toString(),
                  textScaleFactor: 1.3,
                ))
          ]
          ),
          Row(children: <Widget>[
            Container(
                margin: EdgeInsets.only(
                    left: 20, bottom: 10),
                height: 20,
                width: 20,
                decoration: BoxDecoration(
                    borderRadius:
                    BorderRadius.circular(50),
                    color: Colors.lightBlueAccent[200])),
            Container(
                margin: EdgeInsets.only(
                    left: 10, bottom: 10),
                child: Text(
                  "Savings",
                  textScaleFactor: 1.3,
                )),
            Container(
                margin: EdgeInsets.only(
                    left: 10, bottom: 10),
                child: Text('~'+' \$'+(globals.avgMonthlySavings.toInt()).toString(),
                  textScaleFactor: 1.3,
                ))
          ]
          )
        ],
      ),
    );
  }
}
