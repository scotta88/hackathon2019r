import 'package:flutter/material.dart';
import './framework_packages/barchart.dart';
import 'package:charts_flutter/flutter.dart' as charts;

import './framework_packages/lease_table.dart';
import './NavigationDrawer.dart';
class ContractSummary extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ContractSummaryState();
  }
}

class ContractSummaryState extends State<ContractSummary> {
  var averageMonthly = 90;
  var monthDue = "22nd April";
  String dropdownValue = "Loan";
  Widget table = new LeaseTable(true);
   List<OrdinalSales> data;
     List<OrdinalSales> loanData = [
      new OrdinalSales('Jan', 100),
      new OrdinalSales('Feb', 111),
      new OrdinalSales('Mar', 60),
      new OrdinalSales('April', 90),
    ];
     var leaseData = [
      new OrdinalSales('Jan', 258),
      new OrdinalSales('Feb', 123),
      new OrdinalSales('Mar', 468),
      new OrdinalSales('April', 322),
    ];
  

  

 Widget updateTable() {
  
    if (dropdownValue=='Loan') {
      return new LeaseTable(false);
    } else {
     return new LeaseTable(true);
    }
  
}



  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      drawer: NavigationDrawer(),
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
        title: Text("CONTRACT SUMMARY",style: TextStyle(color: Colors.white),),
    backgroundColor: Colors.indigoAccent,
    centerTitle: true,
    ),
        body: ListView(children: <Widget>[
          Container(height: 300, child: SimpleBarChart(_createSampleData())),
          Row(
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(top: 15, left: 15),
                  child: Row(children: <Widget>[
                    Icon(Icons.attach_money),
                    Text(
                      "Monthly installment:",
                      textScaleFactor: 1.5,
                    )
                  ])),
              Container(
                  margin: EdgeInsets.only(top: 15, left: 40),
                  child: Text(
                    "${averageMonthly} \$",
                    textScaleFactor: 1.3,
                  )),
            ],
          ),
          Row(
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(left: 15),
                  child: Row(children: <Widget>[
                    Icon(Icons.date_range),
                    Text(
                      "Due date:",
                      textScaleFactor: 1.5,
                    )
                  ])),
              Container(
                  margin: EdgeInsets.only(left: 140),
                  child: Text(
                    "${monthDue}",
                    textScaleFactor: 1.3,
                  )),
            ],
          ),
          Container(
              margin: EdgeInsets.all(20),
              child: Row(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Row(children: <Widget>[
                        Text(
                          "Select type:",
                          textScaleFactor: 1.4,
                        ),
                        Container(
                            margin: EdgeInsets.only(left: 15),
                            child: DropdownButton<String>(
                                value: dropdownValue,
                                onChanged: (String newValue) {
                                  setState(() {
                                    dropdownValue = newValue;
                                    if (dropdownValue =='Loan'){
                                      data = loanData;
                                         averageMonthly = 90;
                                          monthDue = "22nd April";

                                    }
                                    else{
                                      data = leaseData;
                                       averageMonthly = 256;
                                        monthDue = "25th April";
                                    }
                              
                                  });
                                },
                                items: <String>[
                                  'Loan',
                                  'Lease'
                                ].map<DropdownMenuItem<String>>((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(
                                      value,
                                      textScaleFactor: 1.4,
                                    ),
                                  );
                                }).toList())),
                      ]),
                      
                      Container(width: 350, child: updateTable())
                    ],
                  )
                ],
              ))
        ]));
  }

   List<charts.Series<OrdinalSales, String>> _createSampleData() {
   
  if(dropdownValue=='Loan'){
    data=loanData;
  }else{
    data=leaseData;
  }
    return [
      new charts.Series<OrdinalSales, String>(
        id: 'Sales',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (OrdinalSales sales, _) => sales.month,
        measureFn: (OrdinalSales sales, _) => sales.amount,
        data: data,
      )
    ];
  }


  
}

/// Sample ordinal data type.
class OrdinalSales {
  final String month;
  final int amount;

  OrdinalSales(this.month, this.amount);
}
