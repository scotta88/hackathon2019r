import "package:flutter/material.dart";
import 'package:charts_flutter/flutter.dart' as charts;
import './framework_packages/loan_chart.dart';
import './NavigationDrawer.dart';

class ContractDetailsPage extends StatefulWidget {
  String contractReference;

  ContractDetailsPage(this.contractReference);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ContractDetailsState(contractReference);
  }
}

class ContractDetailsState extends State<ContractDetailsPage> {
  String contractReference;
  double sliderValue = 100;
  int initLoanAmount = 1000;
  int current =0;
  int remaining;
  int maxAvailable;
  int initialInterestAmount;
  int remainingToPay;
  int interestAmount;
  double interestRate =0.1;
  String maturityDate = "28 April 2019";

  ContractDetailsState(this.contractReference);

  @override
  Widget build(BuildContext context) {
    int sliderText = sliderValue.toInt();
    current = (initLoanAmount - sliderValue.toInt());
  
    interestAmount =  (current + (current - (current * 0.9))).toInt();

    remaining = (current / 5).toInt();
    maxAvailable = initLoanAmount - current;
    remainingToPay = (initLoanAmount - sliderValue).toInt();
    String interestRateText = (interestRate * 100).toString();
 
    // TODO: implement build
    return MaterialApp(
        home: Scaffold(
      drawer: NavigationDrawer(),
            appBar: AppBar(
              title: Text(contractReference),
              backgroundColor: Colors.indigoAccent,
              centerTitle: true,
            ),
      body: Column(
        children: <Widget>[
          Container(
              margin: EdgeInsets.only(left: 45, top: 30),
              child: Text(
                "$sliderText \$",
                textScaleFactor: 1.8,
                style: TextStyle(fontFamily: "Verdana"),
              )),
          Container(
            margin: EdgeInsets.only(left: 10),
            width: 370,
            child: Slider(
              activeColor: Colors.indigoAccent,
              min: 0,
              max: initLoanAmount.toDouble(),
              onChanged: (newRating) {
                setState(() {
                  sliderValue = newRating;
                  current = (initLoanAmount - sliderValue.toInt());
                  remaining = (current / 3).toInt();
             
                });
              },
              value: sliderValue,
            ),
          ),
          Container(height: 320, child: LoanChart(_createSampleData())),
          Container(
            margin: EdgeInsets.all(20),
              child: Column(
            children: <Widget>[
             Text("Principal amount: \$${initLoanAmount}",textScaleFactor: 1.4,),
             Text("Interest rate: \$${interestRateText}",textScaleFactor: 1.4),
             Text("Interest amount: \$${interestAmount}",textScaleFactor: 1.4),
             Text("Remaining: \$${remainingToPay}",textScaleFactor: 1.4,textAlign: TextAlign.left),
             Text("Maturity date: ${maturityDate}",textScaleFactor: 1.4),
              
            ]
          )),
        ],
      ),
    ));
  }

  List<charts.Series<LinearSales, int>> _createSampleData() {
    int interestToShow = current-interestAmount;

    var myFakeDesktopData = [
      new LinearSales(0, initLoanAmount),
      new LinearSales(1, current),
      new LinearSales(2, remaining),
      new LinearSales(3, remaining),
      new LinearSales(4, remaining),
      new LinearSales(5, remaining),
      new LinearSales(6, remaining),
    ];

    var myFakeTabletData = [
      new LinearSales(0,
          100),
      new LinearSales(1, interestToShow),
      new LinearSales(2, interestToShow),
      new LinearSales(3, interestToShow),
      new LinearSales(4, interestToShow),
      new LinearSales(5, interestToShow),
      new LinearSales(6, interestToShow),
    ];

    return [
      new charts.Series<LinearSales, int>(
        id: 'Payments',
        // colorFn specifies that the line will be blue.
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        // areaColorFn specifies that the area skirt will be light blue.
        areaColorFn: (_, __) =>
            charts.MaterialPalette.blue.shadeDefault.lighter,
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        data: myFakeDesktopData,
      ),
      new charts.Series<LinearSales, int>(
        id: 'Tablet',
        // colorFn specifies that the line will be red.
        colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
        // areaColorFn specifies that the area skirt will be light red.
        areaColorFn: (_, __) => charts.MaterialPalette.red.shadeDefault.lighter,
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        data: myFakeTabletData,
      ),
    ];
  }
}

/// Sample linear data type.
class LinearSales {
  final int year;
  final int sales;

  LinearSales(this.year, this.sales);
}
