import 'package:flutter/material.dart';
import 'package:financial_assistant/ContractsSummaryPageView.dart';

class ContractsSummaryPage extends StatefulWidget {
  static String tag = 'contract-summary-page';
  @override
  ContractsSummaryPageView createState() => new ContractsSummaryPageView();
}

abstract class ContractsSummaryPageState extends State<ContractsSummaryPage> {}
