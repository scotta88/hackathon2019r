import 'package:flutter/material.dart';
import './NavigationDrawer.dart';
import './framework_packages/budgeting_table.dart';

class RecommendationPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return RecommendationPageState();
  }
}

class RecommendationPageState extends State<RecommendationPage> {
  String dropdownValue = "April";
  List<String> months = [
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "December",
    "November",
    "January"
  ];

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
       // backgroundColor: Colors.teal[50],
      drawer: NavigationDrawer(),
      appBar: AppBar(
        title: Text("Budgeting", style: TextStyle(color: Colors.white)),
        backgroundColor: Colors.indigoAccent,
        centerTitle: true,
      ),
      body: ListView(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 20, top: 30, right: 20),
//            decoration: BoxDecoration(
//                borderRadius: BorderRadius.circular(20),
//                border: new Border.all(color: Colors.black)),
            child: Row(
              children: <Widget>[

                Container(
                    margin: EdgeInsets.only(left: 50),
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.account_balance,color: Colors.blueAccent,size: 65,),

                      ],
                    )),
                Container(
                    margin: EdgeInsets.only(left: 120),
                    child: DropdownButton<String>(
                      value: dropdownValue,
                      onChanged: (String value) {
                        setState(
                          () {
                            dropdownValue = value;
                          },
                        );
                      },
                      items:
                          months.map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(
                            value,
                            textScaleFactor: 1.2,
                          ),
                        );
                      }).toList(),
                    ))
              ],
            ),
          ),
          Container(
              margin: EdgeInsets.only(left: 30),
              child: Row(
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only( top: 15),
//                      child: Center(
//                          child: Text(
//                        "SPENDING PLAN",
//                        textScaleFactor: 2,
//                      ))
                  )
                ],
              )),
          BudgetingTable(),
          Row(children: <Widget>[
            Container(
              width: 150,
                margin: EdgeInsets.only(bottom:20,top:30,left:29),
                child: RaisedButton(color: Colors.blueAccent,
                  child: Text(
                    "Estimate",
                    textScaleFactor: 1.2,
                    style: TextStyle(color: Colors.white)
                  ),
                  onPressed: () {
                  showDialog(
            context: context,
            builder: (BuildContext context) {
              // return object of type Dialog
              return AlertDialog(
                title: new Text("NICE PLAN!"),
                content: new Text("With the above Budget plan, you will save 10% more when compared to the last month."),
                actions: <Widget>[
                  // usually buttons at the bottom of the dialog
                  new FlatButton(
                    child: new Text("Close"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            },
          );
                  },
                )),
            Container(
                width: 150,
                margin: EdgeInsets.only(top:30,bottom:20,left:50),
                child: RaisedButton(
                  color: Colors.blueAccent,
                  child: Text(
                    "Save",
                    textScaleFactor: 1.2,
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {},
                ))
          ]),
        ],
      ),
    );
  }
}
