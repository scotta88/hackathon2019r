import 'package:flutter/material.dart';
import 'package:financial_assistant/ContractLoanPage.dart';
import 'NavigationDrawer.dart';

class ContractLoanPageView extends ContractLoanPageState {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Loan Details',
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            backgroundColor: Colors.blue[100],
            drawer: NavigationDrawer(),
            appBar: AppBar(
              iconTheme: IconThemeData(color: Colors.white),
              title: Text("CONTRACT LOAN",style: TextStyle(color: Colors.white),),
              backgroundColor: Colors.indigoAccent,
              centerTitle: true,
            ),
            body: Column(children: <Widget>[])));
  }
}
