import 'package:flutter/material.dart';
import 'package:financial_assistant/BudgetingPage.dart';
import 'NavigationDrawer.dart';

class BudgetingPageView extends BudgetingPageState {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Budgeting',
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            backgroundColor: Colors.blue[100],
            drawer: NavigationDrawer(),
            appBar: AppBar(
              title: Text("BUDGETING"),
              backgroundColor: Colors.indigoAccent,
              centerTitle: true,
            ),
            body: Column(children: <Widget>[])));
  }
}
