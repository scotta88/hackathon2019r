import 'package:flutter/material.dart';
import 'package:financial_assistant/LoginEmailPageView.dart';
import 'dart:async';
import 'package:mysql1/mysql1.dart';

class LoginEmailPage extends StatefulWidget {
  static String tag = 'login-email-page';
  @override
  LoginEmailPageView createState() => new LoginEmailPageView();
}

abstract class LoginEmailPageState extends State<LoginEmailPage> {
  TextEditingController emailController = TextEditingController();
  TextEditingController pwdController = TextEditingController();

  Future getPwd(String email) async {
    var conn = await getConn();
    var results =
        await conn.query('select password from users where email= ?', [email]);
    await conn.close();
    for (var row in results) {
      return row[0];
    }
  }

  Future getConn() async {
    return await MySqlConnection.connect(new ConnectionSettings(
        host: '10.0.2.2',
        port: 3306,
        user: 'root',
        password: 'root',
        db: 'hackathon'));
  }
}
