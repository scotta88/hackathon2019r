import 'package:flutter/material.dart';
import 'package:financial_assistant/SpendingForecastPageView.dart';
import 'framework_packages/globals.dart' as globals;
import 'package:flutter_circular_chart/flutter_circular_chart.dart';

class SpendingForecastPage extends StatefulWidget {
  static String tag = 'spending-forecast-page';

  @override
  SpendingForecastPageView createState() => new SpendingForecastPageView();
}

abstract class SpendingForecastPageState extends State<SpendingForecastPage> {

  final GlobalKey<AnimatedCircularChartState> chartKey =
  new GlobalKey<AnimatedCircularChartState>();

  List<CircularStackEntry> initialData = <CircularStackEntry>[
    new CircularStackEntry(
      <CircularSegmentEntry>[
        new CircularSegmentEntry(
            628.8571428571429, Colors.green[200], rankKey: 'Q1'),
        new CircularSegmentEntry(
            277.42857142857144, Colors.red[200], rankKey: 'Q2'),
        new CircularSegmentEntry(
            1438.2857142857142, Colors.blue[200], rankKey: 'Q3'),
        new CircularSegmentEntry(350.0, Colors.yellow[200], rankKey: 'Q4'),
        new CircularSegmentEntry(
            185.14285714285714, Colors.deepOrange[200], rankKey: 'Q5'),
        new CircularSegmentEntry(332.75, Colors.brown[200], rankKey: 'Q6'),
        new CircularSegmentEntry(
            498.42857142857144, Colors.blueGrey[200], rankKey: 'Q7'),
        new CircularSegmentEntry(
            1450.5357142857142, Colors.lightBlueAccent[200], rankKey: 'Q8')
      ],
      rankKey: 'Quarterly Profits',
    ),
  ];


//  void cycleSamples() {
//    List<CircularStackEntry> realData = <CircularStackEntry>[
//      new CircularStackEntry(
//        <CircularSegmentEntry>[
//          new CircularSegmentEntry(628.8571428571429, Colors.red[200], rankKey: 'Q1'),
//          new CircularSegmentEntry(498.42857142857144, Colors.green[200], rankKey: 'Q2'),
//        new CircularSegmentEntry(1438.2857142857142, Colors.blue[200], rankKey: 'Q3'),
//        new CircularSegmentEntry(350.0, Colors.yellow[200], rankKey: 'Q4'),
//        new CircularSegmentEntry(185.14285714285714, Colors.amber[200], rankKey: 'Q5'),
//        new CircularSegmentEntry(332.75, Colors.brown[200], rankKey: 'Q6'),
//        new CircularSegmentEntry(498.42857142857144, Colors.blueGrey[200], rankKey: 'Q7'),
//          new CircularSegmentEntry(1450.5357142857142, Colors.lightBlueAccent[200], rankKey: 'Q8')
//
//          new CircularSegmentEntry(globals.avgMonthlySpendOnEducation, Colors.red[200], rankKey: 'Q1'),
//          new CircularSegmentEntry(globals.avgMonthlySpendOnFood, Colors.green[200], rankKey: 'Q2'),
//          new CircularSegmentEntry(globals.avgMonthlySpendOnHousehold, Colors.blue[200], rankKey: 'Q3'),
//          new CircularSegmentEntry(globals.avgMonthlySpendOnInvestment, Colors.yellow[200], rankKey: 'Q4'),
//          new CircularSegmentEntry(globals.avgMonthlySpendOnPersonal, Colors.amber[200], rankKey: 'Q5'),
//          new CircularSegmentEntry(globals.avgMonthlySpendOnShopping, Colors.brown[200], rankKey: 'Q6'),
//          new CircularSegmentEntry(globals.avgMonthlySpendOnTravel, Colors.blueGrey[200], rankKey: 'Q7'),
//          new CircularSegmentEntry(globals.avgMonthlySavings, Colors.lightBlueAccent[200], rankKey: 'Q8')
//        ],
//        rankKey: 'Quarterly Profits',
//      ),
//    ];
//    setState(() {
//      chartKey.currentState.updateData(realData);
//    });
//  }
}


