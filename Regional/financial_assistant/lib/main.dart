import 'package:flutter/material.dart';

import 'AssistantPage.dart';
import 'BudgetingPage.dart';
import 'ContractLeasePage.dart';
import 'ContractLoanPage.dart';
import 'ContractSpendingAdjPage.dart';
import 'ContractSummary.dart';
import 'DashboardPage.dart';
import 'DetailsPage.dart';
import 'LoginEmailPage.dart';
import 'LoginPage.dart';
import 'RegisterPage.dart';
import 'SpendingForecastPage.dart';
import 'SplashPage.dart';

void main() => runApp(FinancialAssistant());

class FinancialAssistant extends StatelessWidget {
  //just put your page in this map, and change the home: {your_own_page} for testing
  final routes = <String, WidgetBuilder>{
    AssistantPage.tag: (context) => AssistantPage(),
    BudgetingPage.tag: (context) => BudgetingPage(),

    ContractLoanPage.tag: (context) => ContractLoanPage(),
    ContractSpendingAdjPage.tag: (context) => ContractSpendingAdjPage(),

    DashboardPage.tag: (context) => DashboardPage(),
    DetailsPage.tag: (context) => DetailsPage(),
    LoginEmailPage.tag: (context) => LoginEmailPage(),
    LoginPage.tag: (context) => LoginPage(),
    RegisterPage.tag: (context) => RegisterPage(),
    SpendingForecastPage.tag: (context) => SpendingForecastPage(),
    SplashPage.tag: (context) => SplashPage(),
  };

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Financial Assistant',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.lightBlue,
        fontFamily: 'Nunito',
      ),
      home: SplashPage(),
      routes: routes,
    );
  }
}
