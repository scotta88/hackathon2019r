import 'package:flutter/material.dart';
import 'package:financial_assistant/DashboardPage.dart';
import 'package:flutter_circular_chart/flutter_circular_chart.dart';
import 'NavigationDrawer.dart';
import 'framework_packages/globals.dart' as globals;

class DashboardPageView extends DashboardPageState {
  var spending;
  var totalSpent = 460;
  var balance = 3540.0;
  double percentageSpent = 0.115;
  @override
  initState() {
    super.initState();
    globals.getAllTransactions();
    spending = getAprilSpendings();
//    cycleSamples(spending[0], spending[1],
//        spending[2], spending[3],
//        spending[4], spending[5],
//        spending[6]);
    totalSpent = spending[0] + spending[1] +
        spending[2] + spending[3]+
        spending[4]+ spending[5]+
        spending[6];
    balance = globals.monthlyIncome - totalSpent;
    percentageSpent = totalSpent/globals.monthlyIncome;
    getUpcomingPayments();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavigationDrawer(),
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        title: Text("DASH BOARD",style: TextStyle(color: Colors.white),),
        backgroundColor: Colors.indigoAccent,
        centerTitle: true,
      ),
      body: CustomScrollView(
        shrinkWrap: true,
        slivers: <Widget>[
          SliverPadding(
            padding: const EdgeInsets.all(1.0),
            sliver: SliverList(
              delegate: SliverChildListDelegate(
                <Widget>[
                  Container(

                      margin: EdgeInsets.fromLTRB(18, 20, 18, 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Expense',
//                            textScaleFactor: 1.8,
                              style: new TextStyle(
                                  fontSize: 23.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black)
                          ),
//                          Container(
//                            margin: EdgeInsets.only(top: 10, bottom: 10),
//                            height: 1.5,
//                            width: 200,
//                            color: Colors.grey[300],
//                          ),
//                          Container(
////                              margin: EdgeInsets.only(top: 0),
//                              decoration: new BoxDecoration(
////                                  borderRadius: BorderRadius.circular(25),
//                                  border: new Border.all(color: Colors.black)),
//                              child:
                              Column(children: <Widget>[
                                Container(
                                    margin: EdgeInsets.all(20),
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Container(
                                              width: 250,
                                              //  width: MediaQuery.of(context).size.width*0.4,
                                              child: Text(
                                                "April",
                                                textScaleFactor: 1.3,
                                              ),
                                            ),
                                            Text(
                                              "\$ " +
                                                  "3540.0",
                                              textScaleFactor: 1.3,
                                              textAlign: TextAlign.right,
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          child: new LinearProgressIndicator(
                                            value: 0.115,
                                            semanticsValue: '0.115',
                                            valueColor:
                                                new AlwaysStoppedAnimation(
                                                    Colors.blue),
                                          ),
                                          height: 23,
                                        )
                                      ],
                                    ))
                              ])
//                          ),
                        ],
                      )),
                  //Container(),
                  Container(
                      margin: EdgeInsets.fromLTRB(18, 20, 18, 0),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[

                        Text(
                          'Statistics',
//                          textScaleFactor: 1.8,
//                           style: TextStyle(fontWeight: FontWeight.bold),textAlign: TextAlign.left,
                            style: new TextStyle(
                                fontSize: 23.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.black)
                        ),
//                        Container(
//                          margin: EdgeInsets.only(top: 10, bottom: 10),
//                          height: 1.5,
//                          width: 200,
//                          color: Colors.grey[300],
//                        ),
//                        Container(
////                            margin: EdgeInsets.only(top: 10),
//                            decoration: new BoxDecoration(
////                                borderRadius: BorderRadius.circular(25),
////                                border: new Border.all(color: Colors.black)
//                            ),
//                            child:
                            Column(
                              children: <Widget>[
                                AnimatedCircularChart(
                                  key: chartKey,
                                  size: const Size(300, 330),
                                  initialChartData: initialChartData,
                                  chartType: CircularChartType.Pie,
                                ),
                                Row(
                                  children: <Widget>[
                                    Container(
                                        margin: EdgeInsets.only(
                                            left: 10, bottom: 10),
                                        height: 20,
                                        width: 20,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                            BorderRadius.circular(50),
                                            color: Colors.red[200])),
                                    Container(
                                        margin: EdgeInsets.only(
                                            left: 10, bottom: 10),
                                        child: Text(
                                          "Education",
                                          textScaleFactor: 1,
                                        )),
                                    Container(
                                        margin: EdgeInsets.only(
                                            left: 10, bottom: 10),
                                        height: 20,
                                        width: 20,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                            BorderRadius.circular(50),
                                            color: Colors.green[200])),
                                    Container(
                                        margin: EdgeInsets.only(
                                            left: 10, bottom: 10),
                                        child: Text(
                                          "Food",
                                          textScaleFactor: 1,
                                        )),
                                    Container(
                                        margin: EdgeInsets.only(
                                            left: 20, bottom: 10),
                                        height: 20,
                                        width: 20,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(50),
                                            color: Colors.blue[200])),
                                    Container(
                                        margin: EdgeInsets.only(
                                            left: 10, bottom: 10),
                                        child: Text(
                                          "House",
                                          textScaleFactor: 1,
                                        )),
                                    Container(
                                        margin: EdgeInsets.only(
                                            left: 20, bottom: 10),
                                        height: 20,
                                        width: 20,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(50),
                                            color: Colors.yellow[200])),
                                    Container(
                                        margin: EdgeInsets.only(
                                            left: 10, bottom: 10),
                                        child: Text(
                                          "Invest.",
                                          textScaleFactor: 1,
                                        ))
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Container(
                                        margin: EdgeInsets.only(
                                            left: 10, bottom: 10),
                                        height: 20,
                                        width: 20,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                            BorderRadius.circular(50),
                                            color: Colors.deepOrange[200])),
                                    Container(
                                        margin: EdgeInsets.only(
                                            left: 10, bottom: 10),
                                        child: Text(
                                          "Personal",
                                          textScaleFactor: 1,
                                        )),
                                    Container(
                                        margin: EdgeInsets.only(
                                            left: 20, bottom: 10),
                                        height: 20,
                                        width: 20,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                            BorderRadius.circular(50),
                                            color: Colors.brown[200])),
                                    Container(
                                        margin: EdgeInsets.only(
                                            left: 10, bottom: 10),
                                        child: Text(
                                          "Shopping",
                                          textScaleFactor: 1,
                                        )),
                                    Container(
                                        margin: EdgeInsets.only(
                                            left: 20, bottom: 10),
                                        height: 20,
                                        width: 20,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                            BorderRadius.circular(50),
                                            color: Colors.blueGrey[200])),
                                    Container(
                                        margin: EdgeInsets.only(
                                            left: 10, bottom: 10),
                                        child: Text(
                                          "Travel",
                                          textScaleFactor: 1,
                                        ))
                                  ],
                                ),
                              ],
                            )
//                        ),
//                        Container(
//                            height: 50,
//                            width: 350,
//                            margin: EdgeInsets.only(top: 25),
//                            decoration: new BoxDecoration(
//                                borderRadius: BorderRadius.circular(10),
//                                border: new Border.all(color: Colors.black)),
//                            child: Container(
//                                margin: EdgeInsets.all(5),
//                                child: Text(
//                                  "Some text",
//                                  textScaleFactor: 2,
//                                )))
                      ])),
                  Container(
                      margin: EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Upcoming',
//                            textScaleFactor: 1.8,
//                             style: TextStyle(fontWeight: FontWeight.bold),textAlign: TextAlign.left,
                              style: new TextStyle(
                                  fontSize: 23.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black)
                          ),
//                          Container(
//                            margin: EdgeInsets.only(top: 10, bottom: 10),
//                            height: 1.5,
//                            width: 200,
//                            color: Colors.grey[300],
//                          ),
                          Container(
                            width: 370,
                              margin: EdgeInsets.only(top: 10),
                              padding: const EdgeInsets.fromLTRB(0, 10, 10, 10),
                              decoration: new BoxDecoration(
                                  //borderRadius: BorderRadius.circular(25),
//                                  border: new Border.all(color: Colors.black)
 ),

                                child: Column(
                                    children: <Widget>[
                                      Text(
                                        'Electricity: \$'+globals.upcomingElectricityBill.toString()+'                     \( Due on 28/04/2019 \)',
                                        textScaleFactor: 1,
                                        textAlign: TextAlign.left,
                                      ),
                                      Padding(padding: EdgeInsets.only(top: 10),
                                      child: Text(
                                        'Rent: \$'+globals.upcomingRentBill.toString()+'                               \( Due on 25/04/2019 \)',
                                        textScaleFactor: 1,
                                        textAlign: TextAlign.left,
                                      ),)

                                    ]),
                              )

                        ],
                      ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
