import 'package:flutter/material.dart';
import 'package:financial_assistant/ContractLoanPageView.dart';

class ContractLoanPage extends StatefulWidget {
  static String tag = 'loan-page';

  @override
  ContractLoanPageView createState() => new ContractLoanPageView();
}

abstract class ContractLoanPageState extends State<ContractLoanPage> {}
