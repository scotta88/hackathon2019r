import 'package:flutter/material.dart';
import 'package:financial_assistant/DetailsPage.dart';
import 'NavigationDrawer.dart';
import 'framework_packages/globals.dart';
import 'framework_packages/DatePicker.dart';
import 'package:intl/intl.dart';

class DetailsPageView extends DetailsPageState {
  String dropdownValue = "All";

  List<String> category = [
    "All",
    "Shopping",
    "Food Liquor",
    "Household",
    "Education",
    "Personal Care",
    "Travel",
    "Investment"
  ];


  @override
  void initState() {
    super.initState();
    // NOTE: Calling this function here would crash the app.
    getTranscations();
    list = transactionsList;
    list.sort();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Details',
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            backgroundColor: Colors.white,
            drawer: NavigationDrawer(),
            appBar: AppBar(
              iconTheme: IconThemeData(color: Colors.white),
              title: Text("DETAILS",style: TextStyle(color: Colors.white),),
              backgroundColor: Colors.indigoAccent,
              centerTitle: true,
            ),
            body: isLoading
                ? Center(
              child: CircularProgressIndicator(),
            )
                : SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    height: 200,
                    margin: EdgeInsets.only(
                        top: 10.0, left: 10.0, right: 10.0),
//                    decoration: BoxDecoration(
//                        border: new Border(
//                            top: BorderSide(width: 2.0, color: Colors.blue),
//                            left: BorderSide(width: 2.0, color: Colors.blue),
//                            right: BorderSide(width: 2.0, color: Colors.blue),
//                            bottom:
//                            new BorderSide(width:2.0, color: Colors.blue))),
                    child: Column(
                        children: <Widget>[
                          DatePicker(
                              labelText: 'From',
                              selectedDate: fromDate,
                              selectDate: (DateTime date) {
                                setState(() {
                                  fromDate = date;
                                  fromController.text =
                                      DateFormat('dd/mm/yyyy').format(date);
                                });
                              }
                          ),
                          DatePicker(
                              labelText: 'To',
                              selectedDate: toDate,
                              selectDate: (DateTime date) {
                                setState(() {
                                  toDate = date;
                                  toController.text =
                                      DateFormat('dd/mm/yyyy').format(date);
                                });
                              }
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                  height: 50,

                                  margin: EdgeInsets.only(left: 0),
                                  child: Row(
                                      children: <Widget>[
                                        Text(
                                          'Category      ',
                                          style: TextStyle(
                                              color: Colors.black54,
                                              fontSize: 15.0),
                                          textAlign: TextAlign.left,
                                        ),
                                        DropdownButton<String>(
                                          value: dropdownValue,
                                          onChanged: (String value) {
                                            setState(
                                                  () {
                                                dropdownValue = value;
                                              },
                                            );
                                          },
                                          items: category.map<
                                              DropdownMenuItem<String>>(
                                                  (String value) {
                                                return DropdownMenuItem<String>(
                                                  value: value,
                                                  child: Text(
                                                    value,
                                                    textScaleFactor: 0.8,
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        fontWeight: FontWeight
                                                            .bold),
                                                  ),
                                                );
                                              }).toList(),
                                        ),
                                        Container(
                                            margin: EdgeInsets.fromLTRB(
                                                130, 10, 0, 0),
                                            child: Column(
                                                children: <Widget>[
                                                  SizedBox(
                                                    width: 60,
                                                    height: 30,
                                                    child: RaisedButton(
                                                      shape: RoundedRectangleBorder(
                                                        borderRadius: BorderRadius
                                                            .circular(1),
                                                      ),
                                                      onPressed: () {
                                                        setState(() {
                                                          list =
                                                              getDataByDateAndCategory(
                                                                  fromController
                                                                      .text,
                                                                  toController
                                                                      .text,
                                                                  dropdownValue);
                                                        });
                                                      },
                                                      color: Colors.blueAccent,
                                                      child: Text('Go',
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .white)),
                                                    ),
                                                  )
                                                ])
                                        ),

                                      ]
                                  )
                              ),

                            ],
                          ),
                        ]
                    ),

                  ),
                  Container(
                      height: 500,
                      decoration: BoxDecoration(
                          border: new Border(
                              top: BorderSide(
                                  width: 1.0, color: Colors.blueGrey)
                          )),
                      child: ListView.builder(
                        itemBuilder: (context, position) {
                          return Column(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Padding(
                                        padding:
                                        const EdgeInsets.fromLTRB(
                                            12.0, 12.0, 12.0, 6.0),
                                        child: Text(
                                          list[position].item,
                                          style: TextStyle(
                                              fontSize: 22.0,
                                              fontWeight:
                                              FontWeight.bold,
                                              color: Colors.blueAccent
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                        const EdgeInsets.fromLTRB(
                                            12.0, 6.0, 12.0, 12.0),
                                        child: Text(
                                          list[position].date,
                                          style:
                                          TextStyle(fontSize: 15.0,
                                              color: Colors.grey),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        Text(
                                          "\-\$" +
                                              list[position]
                                                  .amount
                                                  .toString(),
                                          style: TextStyle(
                                            fontSize: 22.0,
                                          ),
                                        ),
                                        Padding(
                                          padding:
                                          const EdgeInsets.all(8.0),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              Divider(
                                height: 2.0,
                                color: Colors.grey,
                              )
                            ],
                          );
                        },
                        itemCount: list.length,
                      )),
                ],
              ),
            )));
  }
}
