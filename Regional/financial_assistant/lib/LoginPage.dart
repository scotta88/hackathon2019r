import 'package:flutter/material.dart';
import 'package:financial_assistant/LoginPageView.dart';
import 'framework_packages/auth.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'RegisterPage.dart';
import 'DashboardPage.dart';
import 'package:google_sign_in/google_sign_in.dart';

class LoginPage extends StatefulWidget {
  static String tag = 'login-page';

  @override
  LoginPageView createState() => new LoginPageView();
}

abstract class LoginPageState extends State<LoginPage> {
  TextEditingController emailController = TextEditingController();
  TextEditingController pwdController = TextEditingController();

  final Auth auth = new Auth(
      firebaseAuth: FirebaseAuth.instance, googleSignIn: GoogleSignIn());

  void handleSignIn(BuildContext context) {
    try {
      auth.signInWithGoogle().then((FirebaseUser user) {
        if (_existingUser()) {
          Navigator.pushNamed(context, DashboardPage.tag);
        } else {
          Navigator.pushNamed(context, RegisterPage.tag);
        }
      });
    } on Exception catch (e) {
      Navigator.pushNamed(context, DashboardPage.tag);
    }
  }

  bool _existingUser() {
    return true;
  }
}
