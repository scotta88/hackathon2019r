# Financial Assistant
Financial Assistant is a mobile platform application created to allow users to track their Conract Details, Transactions, Budgets and Spending Forecasts and help in identifying areas that can help users with financial well being and achieving their financial goals, being paying off student loan. Saving for a house, or looking to lease a new laptop.

## Motivation Behind this Creation
The banking landscape is changing. Millennials are now surpassing baby boomers as the largest generation in the US with an estimated $30 trillion in wealth transferring to them over the next 30 years.

Consumer visits to retail bank branches are set to drop 36% between 2017 and 2022, with mobile transactions rising 121% in the same period, as customers increasingly shift to accessing their banking information via apps and secure, responsive sites on their mobile devices.
Younger Millennials between the ages of 18 and 24 will visit their bank around six times this year, but this will dip to just two visits annually by 2022.

According to the 2018 Mobile Banking Study from Deloitte which revealed that 46 percent of U.S. consumers have increased their use of mobile banking in the past year. Eight out of 10 people (81 percent) now use their phone to manage their money on nine days in every month, on average.

![Image](Images/Deloitte.jpg)

Customers Switching Banks

---

According to the latest stats, there are around 70 million deposit accounts across the UK, with the average citizen having 2.4 accounts, and a million account switches a year. That�s about 1.4% who switch.
The below statistic presents the volume of bank account switching listed for leading United Kingdom (UK) banks by customers gained and lost in the third quarter of 2018.

![Image](Images/Bank.jpg)

## Technologies & Arcthitecture Used
### Front End UI:
We used Flutter, an open source mobile application development framework created by google in 2015 to develop the front end of the mobile UI.


### Predictive Analytics using Machine Learning
We used Machine Learning models to automatically categorise and predict future spendings and identify the areas that can help improve users personal finances.
We created a machine learning algorithm using the existing models from Azure studio to analyse the transactional details and predict the spending forecast.
 
![Screenshot](Images/MachineLearning.JPG.png)

### Database
We used Firebase as our backend database service. The Firebase Realtime database is a cloud hosted database. Data is stored in JSON and synchronized in realtime to every connected client and remains available when the app goes offline.

Arechitecture Diagram:
![Image](Images/Database.jpg)

### Architecture Used
We used Google Cloud Platform App Engine to generate API's, client Libraries in the back end and used 'Firebase' as a Database with the mobile application serving as front end UI.

Arechitecture Diagram:
![Image](Images/App%20Architecture.jpg)


# What is the code designed for
The code written for this application is desgined to fulfill the below requirements,
1. Provide a easily navigatable user friendly moble app UI.
2. Hook to the machine learning algorithm using REST API calls.
3. Provide realtime transactional details.
4. Provide voice/chat assistance for the mobile payments and to know the budget/spending details.
5. Provide Contract summary details along with the recommendations based on the transactions using machine learning.
6. Provide secure authentication for the payments and the login.


# Code Written In
The entire code is written using 'Dart' programming language and Android Studio using Flutter.
https://www.dartlang.org/

# Open Source or Proprietary Software Used
We used Flutter open source mobile application framework for developing this application.

And Azure microsoft studio for developing and training the machine learning model.

# Why is our App cool
Below are some of the cool factors of the app,
1. Financial Assistant provides realtime view of user's financial status helping them to manage the financials in a secured way.
2. Financial Assistant is created with flutter, a new mobile app technology which provides an attractive and user friendly UI with some cool animations and graphics.
3. While this seems mainly aimed at benefiting the end client with ways to pay less interest on loans and be more fiscally responsible, banks have the potential to fully utilize this to their own benefits. 
4. By harnessing Financial Assistants machine learning spending predictions banks will be able to better understand their clients spending habits and identify times and areas where targeted promotions are likely to be accepted or to identify customers that predicted spending suggests they are likely to default and put management plans in place earlier to restrict potential losses. 
5. In order to increase speed to market of Financial Assistant it was built on the flutter mobile platform using Dart, allowing for single code sources shared across android and iOS with OS specific functionality such as the speech recognition seamlessly integrating into the framework and app.
6. This is all hosted within the google ecosystem via google cloud services allowing for the platform to not only be secure but offers smart business analytics, serverless solutions and a consistent experience for end users each time.
7. To ease management and deployment of the app, google firebase has been used for authentication, natural language processing and storage of the result of transaction categorization from the machine learning process.
8. Google firebase also allows for easy expansion of multiple Authentication methods for the app, including options such as Facebook, Microsoft, twitter on top of the email and google account authentications currently in use.
9. Using Firebase will give us the ability to use the Google Cloud Platform Authentication to provide security layers for the application for the login and the payments.
10. On top of the google eco system, we also employed the use of Azures machine learning platform to build two separate models in order to initially categorize the existing datasets and to then look for spending patterns within the datasets to predict regular payments and variances in these or transactions blocks that can impact or relate to each other.
11. Can be used by Banks, Financial Institutions & End users.
12. To enable current access to transactional data and payments we are currently using an open banking platform, this can easily be changed to use a bank or financial institutions own APIs. 

